PImage img;

void setup() {
  size (384,576);
  img = loadImage("/tmp/image1.jpg");
  img.resize(0,576);
  background(200);
  ellipseMode(RADIUS);
  noStroke();
}
int index(float x, float y) {
  int index = (floor(x)+floor(y)*img.width);
  return index;
}
/*receives two points and returns the color in the middle of them*/
color pickColor(float xc, float yc, float x, float y){
  color c;
  float xm = (x+xc)/2.0;
  float ym = (y+yc)/2.0;
  if (xm < 0) xm = 0;
  else if (xm >= img.width) xm = img.width-1;
  if (ym < 0) ym = 0;
  else if (ym >= img.height) ym = img.height-1;
  img.loadPixels();
  c = img.pixels[index(xm,ym)];
  img.updatePixels();
  return c;
}
void lineCircle(float xc, float yc, float radius){
  float x; 
  float y;
  float step = TWO_PI/1000;
  for (float theta = 0; theta < TWO_PI; theta+=step) {
    x = radius*cos(theta);
    y = radius*sin(theta);
    stroke(pickColor(xc,yc, xc+x,yc+y));
    line (xc,yc, xc+x,yc+y);
  }
}

void draw(){
  int r = 5;
  int p = 15;
  int n = 100;
  img.loadPixels();
  for (int i = 0; i < n; ++i){
    //fill(pickColor(i-r,j-r,i+r,j+r));
    if (random (1) < 0.01) p = 100; else p = 15;
    lineCircle (random(img.width), random(img.height), random(r,p));
  }
  img.updatePixels();
}
